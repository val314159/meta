SOLC:=solc-0.7.6
SOLC+=-oout --overwrite --abi --bin
SOLC+=--optimize --no-color
call: clean all
all:
	${SOLC} contracts/*.sol
clean:
	rm -fr out
#	find .
