if [ "" == "$PORT" ]; then
    PORT=19999
fi
docker run -w`pwd` -v`pwd`/..:`pwd`/.. --rm -it -e PORT=$PORT --name lk val314159/lk:dev /usr/local/lordkelvin/tools/launch_ganache.py $PORT bash -l
